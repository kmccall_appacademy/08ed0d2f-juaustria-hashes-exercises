# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  str.split(' ').each_with_object({}) do |string, hsh|
    hsh[string] = string.length
  end
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |_k, v| v }.last.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  counter_hash = Hash.new(0)
  word.chars { |char| counter_hash[char] += 1 }
  counter_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  element_count = Hash.new(0)
  arr.each { |el| element_count[el] += 1 }
  element_count.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  parity_count = Hash.new(0)
  numbers.each do |num|
    if num.odd?
      parity_count[:odd] += 1
    else
      parity_count[:even] += 1
    end
  end
  parity_count
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
VOWELS = %w[a e i o u].freeze
def most_common_vowel(string)
  vowel_count = Hash.new(0)
  string.chars do |char|
    vowel_count[char] += 1 if VOWELS.include?(char.downcase)
  end
  vowel_count.sort.each do |el|
    return el[0] if el[1] == vowel_count[greatest_key_by_val(vowel_count)]
  end
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  late_birthdays = []
  students.each { |k, v| late_birthdays << k if v > 6 }
  late_birthdays.combination(2)
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  specimens_count = Hash.new(0)
  specimens.each { |specimen| specimens_count[specimen] += 1 }
  number_of_species = specimens_count.keys.length
  pop_size_array = specimens_count.values.sort
  smallest_population_size = pop_size_array.first
  largest_population_size = pop_size_array.last
  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_characters = character_count(normal_sign)
  vandalized_characters = character_count(vandalized_sign)
  vandalized_characters.each do |k, v|
    return false if v > normal_characters[k]
  end
  true
end

def character_count(str)
  characters_hash = Hash.new(0)
  str.gsub(/\W/, '').chars.each { |char| characters_hash[char.downcase] += 1 }
  characters_hash
end
